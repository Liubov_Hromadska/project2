// #1
function extractCurrencyValue(stringCurrency) {
    if(stringCurrency.length >= 16){
        return BigInt(stringCurrency.replace(/[,EUR]/g, ''));
    } 
    return Number(stringCurrency.replace(/[,USD]/g, ''));
}


console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) {
    for (let objProp in obj) {
        if (obj[objProp] === false || obj[objProp] === null || obj[objProp] === undefined) {
            delete obj[objProp];
        }
    }
    return obj
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    let uniqueId = Symbol(param);
    /* let uniqueString = uniqueId.toString(); */
    return uniqueId
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
    const minuteInMs = 1000 * 60;
    const hourInMs = minuteInMs * 60;
    const dayInMs = hourInMs * 24;
    const weekInMs = dayInMs * 7;
    const monthInMs = weekInMs * 4;
    let timeDiffInMs = Date.parse(endDate) - Date.parse(startDate);
    let countDays = Math.round(timeDiffInMs/dayInMs);
    let countWeeks = Math.round(timeDiffInMs/weekInMs);
    let countMonths = Math.round(timeDiffInMs/monthInMs);
    return console.log(`The difference between dates is: ${countDays} day(-s), ${countWeeks} week(-s), ${countMonths} 
    month(-s)`);
}

console.log(countBetweenTwoDays('03/22/22', '05/25/22')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
    const noDuplicatesWithSet = new Set(arr);
    const unsetToArray = [...noDuplicatesWithSet];
    return unsetToArray;
}

console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]


const filterArrayWithoutSet = (arr) => {
    return arr.filter((el, id) => arr.indexOf(el) === id);
}

console.log(filterArrayWithoutSet([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
