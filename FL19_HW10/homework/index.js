document.getElementById('start').onclick = function() {
    const nickname = document.getElementById('nickname').value;
    let count = 0;
    try {
        if(nickname === ''){
            throw new Error('Empty nickname')
        }
    }catch (error) {
        alert(`Empty nickname`)
    }
    
    if(nickname !== ''){
        document.getElementById('clickMe').onclick = function() {

            count++;
            let userData = {
                nickname,
                click: count
            }
            sessionStorage.setItem('userData',JSON.stringify(userData));
        
        };

        setTimeout(function (){
            alert(`You clicked ${count} times`);
        }, 5000)

        console.log(sessionStorage.getItem('userData'));
        /* let storedClicks = JSON.parse(sessionStorage.getItem('userData'));
        console.log(storedClicks)
        delete storedClicks.nickname;
        console.log(storedClicks);
        let arrClick = [];
        arrClick.push(Object.values(storedClicks));
        console.log(arrClick); */
    }

    document.getElementById('best-result').onclick = function() {
        sessionStorage.getItem('userData');
        
        alert(`Best result is ${count}`)
    }

    document.getElementById('best-result-of-all').onclick = function() {
        sessionStorage.getItem('userData');
        
        alert(`Best result of all is ${count}`)
    }
    
    document.getElementById('clear-best-result').onclick = function() {
        sessionStorage.clear();
        
        alert(`Clear best result`)
    }

    document.getElementById('clear-best-result-of-all').onclick = function() {
        sessionStorage.clear();
        
        alert(`Clear best result of all`)
    }
    
}
