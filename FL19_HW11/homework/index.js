// Your code goes here

function getWeekDay(dateObj) {
    let dayLong;

    if (dateObj !== new Date()){
        let convertMs = new Date(dateObj);
        dayLong = convertMs.toLocaleDateString('en-US', { weekday:'long' });
    }else {
        dayLong = dateObj.toLocaleDateString('en-US', { weekday:'long' });
    }

    return dayLong;

}

console.log(getWeekDay(Date.now())); // "Thursday" (if today is the 22nd October)
console.log(getWeekDay(new Date(2020, 9, 22))); // "Thursday"

function getAmountDaysToNewYear() {

    let dateToday = new Date();
    let newYear = new Date(dateToday.getFullYear()+1, 0, 1);
    let dayInMs = 1000*60*60*24;

    return console.log(Math.ceil((newYear.getTime()-dateToday.getTime())/dayInMs));
}
getAmountDaysToNewYear(); 

const birthday17 = new Date(2004, 11, 29); 
const birthday15 = new Date(2006, 11, 29); 
const birthday22 = new Date(2000, 9, 22); 

function getApproveToPass(birthday) {
    let today = new Date();
    let yearInMs = 1000*60*60*24*365;
    let bDayThisYear = birthday.getMonth();
    let monthTilltaoday = today.getMonth();
    let yearIfApprove = Math.ceil((today.getTime()-birthday.getTime())/yearInMs);
    let monthToApprove = bDayThisYear - monthTilltaoday;

    if (yearIfApprove >= 18 && monthToApprove >= 11) {
        return console.log(`Hello adventurer, you may pass!`);
    }else if (yearIfApprove === 18 && monthToApprove < 11) {
        return console.log(`Hello adventurer, you are to yang for this quest wait for few more months!`);
    }else if (yearIfApprove < 18){
        let yearsLeft = 18 - yearIfApprove;
        return console.log(`Hello adventurer, you are to yang for this quest wait for ${yearsLeft} years more!`);
    }
    
}

getApproveToPass(birthday15);

let elemDiv = 'tag="div" class="main" style={width: 50%;} value="Hello World!"';
function transformStringToHtml(el) {

    let element = el.match(/"$/gi);
    element.replace('</div>');
    return console.log(element);
}

transformStringToHtml(elemDiv);
// ‘<div class=”main” style=”width: 50%;”>Hello World!</div>’