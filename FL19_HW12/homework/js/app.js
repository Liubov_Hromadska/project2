import { dictionary } from './dictionary.js';
let rightGuess = dictionary[Math.floor(Math.random() * dictionary.length)]
const numOfGuess = 6;
let currentAttempt = [];
let attemptsRemaining = numOfGuess;
console.log(rightGuess)

function gameBoard() {
    let board = document.getElementById('game-board');

    for (let i = 0; i < numOfGuess; i++) {
        let row = document.createElement('div')
        row.className = 'row-let'
        
        for (let j = 0; j < 5; j++) {

            let box = document.createElement('input')
            box.className = 'box-let';
            row.appendChild(box)
            /* box.setAttribute('contenteditable', 'true') */
            box.setAttribute('maxlength', '1');
        }
        board.appendChild(row)
        
    }
    
}

gameBoard()

document.addEventListener('keyup', (e) => {

    if (attemptsRemaining === 0) {
        return
    }

    let pressedKey = String(e.box)

    document.getElementById('check-btn').addEventListener('click', function () {
        checkGuess();
    })

    let found = pressedKey.match(/[a-z]/gi)
    if (!found || found.length > 1) {
        return
    } 

    document.getElementById('reset-btn').addEventListener('click', function () {
        attemptsRemaining = 0;
    })
})



function checkGuess () {
    let row = document.getElementsByClassName('row-let')[6 - attemptsRemaining]
    let guessString = ''
    let correctGuess = Array.from(rightGuess)

    for (const val of currentAttempt) {
        guessString += val
    }

    if (!dictionary.includes(guessString)) {
        alert('Word not in list!')
        return
    }
   
    for (let i = 0; i < 5; i++) {
        let lettColor = ''
        let box = row.children[i]
        let lett = currentAttempt[i]
        
        let letterPosition = correctGuess.indexOf(currentAttempt[i])
        if (letterPosition === -1) {
            lettColor = 'grey'
        } else {
            if (currentAttempt[i] === correctGuess[i]) {
                lettColor = 'green';
            } else {
                lettColor = 'yellow';
            }

            correctGuess[letterPosition] = '#'
        }
        box.style.backgroundColor = lettColor;
        changeColor(lett, lettColor);
    }

    if (guessString === rightGuess) {
        alert(`Congratulations! You won`);
        attemptsRemaining = 0;
        return;
    } else {
        attemptsRemaining -= 1;
        currentAttempt = [];

        if (attemptsRemaining === 0) {
            alert(`Game over!`);
        }
    }
}

function changeColor(lett, color) {
    for (const elem of document.getElementsByTagName('input')) {
        if (elem.textContent === lett) {
            let oldColor = elem.style.backgroundColor
            if (oldColor === 'green') {
                return
            } 

            if (oldColor === 'yellow' && color !== 'green') {
                return
            }

            elem.style.backgroundColor = color
            break
        }
    }
}



