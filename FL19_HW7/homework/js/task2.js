// Your code goes here
let prize = 0;
let gameCount = 0;

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function resetCounter() {
    gameCount = 0;
    prize = 0;
}

function playGame(
    minNumber = 0,
    maxNumber = 8,
    confirmMessage = 'Do you want to play a game?',
    canceledMessage = 'You did not become a billionaire, but can.'
    ) {
    if (confirm(confirmMessage)) {
        gameCount++;
        let randNumb = randomNumber(minNumber, maxNumber);
        console.log('Right number: ', randNumb); //for test
        let maxAttempt = 3;
        for (let i = 1; i <= maxAttempt; i++) {
            let currentAttempt = 0;
            if (i === 1) {
                currentAttempt = '100'
            }
            if (i === 2) {
                currentAttempt = '50';
            }
            if (i === 3) {
              currentAttempt = '25';
            }
            let n1 = +prompt(
                `Choose a number from 0 to ${maxNumber}
                Attempts left: ${maxAttempt - i + 1}
                Total prize: ${prize}$
                Possible prize of current attempt: ${currentAttempt}$`
            );
            if (Number.isInteger(n1) && n1 >= minNumber && n1 <= maxNumber) {
                if (randNumb === n1) {
                if (i === 1) {
                    prize = prize + 100;
                }
                if (i === 2) {
                    prize = prize + 50;
                }
                if (i === 3) {
                    prize = prize + 25;
                }
                
                playGame(
                    0,
                    8 + gameCount * 4,
                    `Congratulation, you won! Your prize is: ${prize}$. Do you want to continue?`,
                    `Thank you for your participation. Your prize is: ${prize}$`
                );
                resetCounter()
                playGame(0, 8, `Do you want to play again?`);
                return;
            }
        } else {
 alert('Invalid input data'); 
}
    }
    alert(`Thank you for your participation. Your prize is: ${prize}$`);
    resetCounter()
    playGame(0, 8, `Do you want to play again?`);
    } else {
    alert(canceledMessage);
    }
    
}
playGame();