// #1
function calculateSum(arr) {
    let totalCount = 0;
    
    for (let element of arr) { 
        totalCount += element;
    }

    return totalCount;
} 

console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {  
    if (a+b>c && a+c>b && b+c>a) {
        return true
    }
    if(a<=0 || b<=0 || c<=0){
        return false
    }else { 
        return false; 
    }
}

console.log(isTriangle(5,6,7)); //true
console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
    return !/(.).*\1/.test(word);
}

console.log(isIsogram('Dermatoglyphics')); //true
console.log(isIsogram('abab')); //false

// #4
function isPalindrome(word) {
    let wordLength = word.length;
    let regCheck = /[\W_]/gi;
    word = word.toLowerCase().replace(regCheck, '');

    for (let i = 0; i < wordLength/2; i++) {
        if (word[i] !== word[wordLength - 1 - i]) { 
            return false;
        }
    }
    return true;
}

console.log(isPalindrome('Dermatoglyphics')); //false
console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {

    let month = dateObj.toLocaleString('en-US', { month: 'long' }); 
    let day = dateObj.getDate();
    let year = dateObj.getFullYear();

    let formattedDate = `${day} of ${month}, ${year}`;
    return formattedDate;

}

console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => {
    return str.split(`${letter}`).length - 1
}

console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
    let fruitsCount = arr.reduce(function(previuosFruit, currentFruit) {
        previuosFruit[currentFruit] = (previuosFruit[currentFruit] || 0) + 1;
        return previuosFruit;
    }, {});
    return fruitsCount;
}

console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
    let changeBin = arr.reduce((prevVal, curVal) => {
           return prevVal << 1 | curVal;
    });
    return changeBin;
}

console.log(calculateNumber([0, 1, 0, 1])); //5
console.log(calculateNumber([1, 0, 0, 1])); //9
